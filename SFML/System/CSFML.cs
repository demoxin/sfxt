using System;
using System.Runtime.InteropServices;

namespace SFML.System
{
    public static class CSFML
    {
        public const string audio = "csfml-audio-2.dll";
        public const string graphics = "csfml-graphics-2.dll";
        public const string system = "csfml-system-2.dll";
        public const string window = "csfml-window-2.dll";
    }
}
